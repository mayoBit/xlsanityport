package com.xl.Instructor;

import com.pearson.test.eselenium.framework.core.UIDriver;
import com.pearson.test.eselenium.framework.exceptions.TimeOutException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

public class CommonAlertsPageObjects extends InstructorHomePage{
    public CommonAlertsPageObjects(UIDriver driver) {
        super(driver);
    }
    private By alerts = By.cssSelector(".alert-container .css-6bx4c3");
    public By getAlerts() {
        return alerts;
    }



    private By closeIcon = By.cssSelector(".pe-container .build-and-organize-page-component .alert-container .css-1mhau9w>span>div .css-exhbb1");
    public By getCloseIcon() {
        return closeIcon;
    }

    public List<WebElement> getAllAlertList(){
        List<WebElement> alertList = new ArrayList<>();

        try {
            if(uiDriver.findElement(alerts).isDisplayed()){
                logInstruction("LOG INSTRUCTION: GET ALERT LIST");
                alertList = uiDriver.findElements(alerts);
                logInstruction("LOG INSTRUCTION: VERIFIED ALERT LIST");
            }
            else{
                Assert.fail("Alert Not Displayed");
            }

        } catch (TimeOutException | WebDriverException e) {
            logInstruction(
                    "GET ALERT LIST FAILED. \nMETHOD: getAlertList.\n"
                            + e.getLocalizedMessage());
            Assert.fail(e.getLocalizedMessage());
        }
        return alertList;
    }
    public String getTextMinMaxPageAlert(int index){
        String messageText = null;

        try {
            logInstruction("LOG INSTRUCTION: GET TEXT OF MIN MAX PAGE ALERT");
            messageText = this.getAllAlertList().get(index).getText();
            logInstruction("LOG INSTRUCTION:TEXT OF MIN MAX PAGE ALERT VERIFIED");
        } catch (TimeOutException | WebDriverException e) {
            logInstruction(
                    "GET TEXT OF MIN MAX PAGE ALERT FAILED. \nMETHOD: getTextMinMaxPageAlert.\n"
                            + e.getLocalizedMessage());
            Assert.fail(e.getLocalizedMessage());
        }
        return messageText;
    }
    public String getTextMinPriceAlert(int index){
        String messageText = null;

        try {
            logInstruction("LOG INSTRUCTION: GET TEXT OF MIN PRICE ALERT");
            uiDriver.sleep(3000);
            messageText = this.getAllAlertList().get(index).getText();
            logInstruction("LOG INSTRUCTION:TEXT OF MIN PRICE ALERT VERIFIED");
        } catch (TimeOutException | WebDriverException e) {
            logInstruction(
                    "GET TEXT OF MIN PRICE ALERT FAILED. \nMETHOD: getTextMinMaxPageAlert.\n"
                            + e.getLocalizedMessage());
            Assert.fail(e.getLocalizedMessage());
        }
        return messageText;
    }
    public String getTextLimitedBidingOptionsALert(int index){
        String messageText = null;

        try {
            logInstruction("LOG INSTRUCTION: GET TEXT OF LIMITED BINDING OPTIONS ALERT");
            messageText = this.getAllAlertList().get(index).getText();
            logInstruction("LOG INSTRUCTION:TEXT OF LIMITED BINDING OPTIONS ALERT");
        } catch (TimeOutException | WebDriverException e) {
            logInstruction(
                    "GET TEXT OF LIMITED BINDING OPTIONS ALERT \nMETHOD: getTextLimitedBidingOptionsALert.\n"
                            + e.getLocalizedMessage());
            Assert.fail(e.getLocalizedMessage());
        }
        return messageText;
    }

    public String getTextHarvardAndIveyALert(int index){
        String messageText = null;

        try {
            logInstruction("LOG INSTRUCTION: GET TEXT OF HARVARD & IVEY RESTRICTION ALERT");
            messageText = this.getAllAlertList().get(index).getText();
            logInstruction("LOG INSTRUCTION:TEXT OF HARVARD & IVEY RESTRICTION ALERT");
        } catch (TimeOutException | WebDriverException e) {
            logInstruction(
                    "GET TEXT OF HARVARD & IVEY RESTRICTION ALERT\nMETHOD: getTextHarvardAndIveyALert.\n"
                            + e.getLocalizedMessage());
            Assert.fail(e.getLocalizedMessage());
        }
        return messageText;
    }
    public String getTextInAlertCommon(int index){
        String messageText = null;

        try {
            logInstruction("LOG INSTRUCTION: GET TEXT OF VALIDATION ALERT");
            uiDriver.sleep(3000);
            messageText = this.getAllAlertList().get(index).getText();
            logInstruction("LOG INSTRUCTION:TEXT OF  ALERT VERIFIED");
        } catch (TimeOutException | WebDriverException e) {
            logInstruction(
                    "GET TEXT OF ALERT FAILED. \nMETHOD: getTextInAlertCommon.\n"
                            + e.getLocalizedMessage());
            Assert.fail(e.getLocalizedMessage());
        }
        return messageText;
    }
    public String  coverPopupMessageDescription(int index){
        String warningText = null;
        try{
            logInstruction("VALIDATING COVER TRIMSIZE MISMATCH MESSAGE");
            uiDriver.sleep(3000);
            warningText=this.getAllAlertList().get(index).getText();


        }catch (TimeOutException | WebDriverException e){
            logInstruction("TRIMSIZE MISMATCH POPUP MESSAGE NOT DISPLAYED\nMETHOD: coverPopupMessageDescription.\n"
                    + e.getLocalizedMessage());
            Assert.fail(e.getLocalizedMessage());
        }
        return warningText;


    }
    public boolean isAlertDisplayed(int index){
        boolean minMaxAlert = false;
        try {
            logInstruction("LOG INSTRUCTION: VERIFY  ALERT DISPLAY");
            if(getAllAlertList().size()!=0){
                minMaxAlert = this.getAllAlertList().get(index).isDisplayed();
                logInstruction("LOG INSTRUCTION:VERIFIED  ALERT DISPLAY");
            }
            else {
                Assert.fail("No ALert displayed");
            }

        } catch (TimeOutException | WebDriverException e) {
            logInstruction(
                    "PAGE ALERT DISPLAY FAILED. \nMETHOD: isAlertDisplayed.\n"
                            + e.getLocalizedMessage());
            Assert.fail(e.getLocalizedMessage());
        }
        return minMaxAlert;
    }

    public List<WebElement> closeIcons(){
        List<WebElement> closeIcons = new ArrayList<>();
        try {
            logInstruction("LOG INSTRUCTION: VERIFY ALERT MESSAGE CLOSE ICON ");
            closeIcons = uiDriver.findElements(closeIcon);
            uiDriver.sleep(3000);

        } catch (TimeOutException | WebDriverException e) {
            logInstruction(
                    "FAIL TO VERIFY ALERT MESSAGE CLOSE ICON \nMETHOD: closeIcons..\n"
                            + e.getLocalizedMessage());
            Assert.fail(e.getLocalizedMessage());
        }
        return closeIcons;
    }

    public void closeAlert(int index){
        try {
            logInstruction("LOG INSTRUCTION: CLICK CLOSE ICON");
            this.closeIcons().get(index).click();
            uiDriver.sleep(5000);
            logInstruction("LOG INSTRUCTION: ALERT CLOSED");
        } catch (TimeOutException | WebDriverException e) {
            logInstruction(
                    "FAIL TO CLICK CLOSE ICON \nMETHOD: closeAlert..\n"
                            + e.getLocalizedMessage());
            Assert.fail(e.getLocalizedMessage());
        }
    }
}
