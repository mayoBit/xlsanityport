package com.xl.Instructor;

import io.netty.handler.timeout.TimeoutException;

import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.ImplicitlyWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.pearson.test.eselenium.framework.core.UIDriver;
import com.pearson.test.eselenium.framework.core.UIElement;
import com.pearson.test.eselenium.framework.exceptions.TimeOutException;
import com.pearson.test.eselenium.framework.util.UIType;
import com.thoughtworks.selenium.webdriven.commands.Click;


public class HomeworkPage extends CommonAlertsPageObjects {
	
	

//	private UIElement myCollectionLabel = createElement(UIType.Xpath,"//*[@id='content-area']/div/div[2]/div[1]/div[1]/h1/span");
//	private By resourceResultList = By.xpath("//*[@id='content-area']/div/div[2]/div[2]/div[2]/div/div[2]/div[3]/div/div");
	
//	private By addCollectionButton =By.id("addToCollectionBtn");
//	
//	private By ResourceCatogaryList = By.xpath("//*[@id='content-area']/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/div/div/ul");
//	private By resourceByChapter = By.cssSelector(".autoCompleteContainer>.dropDown>div>.listview>li:nth-child(2)");
//
//	private By resourcePriceLabel = By.cssSelector(".pe-copy.listView-price>span>span");
//	private By resourceListDropDown = By.cssSelector(".autoCompleteContainer>.dropDown>div:nth-child(1)>ul>li");

//	private By paginationNextBtn = By.cssSelector(".pe-col-12 .paginationComponent>ul>li:nth-child(7)");
//	private By searchResultsCheckBox = By.cssSelector(".pe-col-12 .resultContainer .listResultSet .listResultsDiv .ccb-result-tile .ccb-row .checkbox .pe-checkbox");
//	private By searchResultsTitle = By.cssSelector(".pe-label .LinesEllipsis");
//	private By searchResultsCheckBoxState = By.cssSelector(".pe-col-12 .resultContainer .listResultSet .listResultsDiv .ccb-result-tile .ccb-row .checkbox .pe-checkbox>input");
//	public By getHarvardIveyRestrictionsModal() {
//		return harvardIveyRestrictionsModal;
//	}
//
//	private By harvardIveyRestrictionsModal = By.cssSelector(".harvard-ivey-restrictions-modal");
//	private By harvardIveyRestrictionsModalHeader = By.cssSelector(".harvard-ivey-restrictions-modal>h2");
//	private By harvardIveyRestrictionsErrorMessage = By.cssSelector(".errorMsg>p>span");
//	private By harvardIveyRestrictionsModalCancel = By.cssSelector(".pe-btn--btn_large.cancel");
//	private By harvardIveyRestrictionsModalAdd = By.cssSelector(".pe-btn--btn_large.addButton");
//	private By restrictionAlert = By.cssSelector("#alert-Error-0");
//	private By restrictionAlertText = By.cssSelector(".alert-content-container .alert-content-");
//	private By restrictionAlertCloseIcon = By.cssSelector(".pe-icon--btn>.error-svg>svg");
//	private By collectionNameLink = By.cssSelector(".header-search.header-title");
//	private By mixingRestrictionContainer = By.cssSelector(".mixing-restriction-container");
//	private By mixingRestrictionContainerTitle = By.cssSelector(".mixing-restriction-container>h2");
//	private By mixingRestrictionContainerContents = By.cssSelector(".mixing-restriction-container>p");
//	private By mixingRestrictionContainerToggleLink = By.cssSelector(".mixing-restriction-container>a");
//	private By violoatedResourceName = By.cssSelector(".resourceName");
//	private By mixingRestrictionContainerOkButton = By.cssSelector(".pe-btn--btn_large.restrictionBtn");
//	private By resourcePlayer = By.cssSelector(".resource-player");
//	private By searchBarInSearchPage = By.id("searchBarId");
//
//
//	private By searchResultListView = By.cssSelector(".list-main >li:nth-child(1)");
//	public By getSearchResultListView() {
//		return searchResultListView;
//	}
//
//	private By searchResultTileView = By.cssSelector(".list-main >li:nth-child(2)");
//	public By getSearchResultTileView() {
//		return searchResultTileView;
//	}
//
//
//
//	private By searchTileView = By.cssSelector(".resultContainer .gridResultSet .grid-item");
//	public By getSearchTileView() {
//		return searchTileView;
//	}
//
//	private By searchListView = By.cssSelector(".listResultSet .listResultsDiv");
//	public By getSearchListView() {
//		return searchListView;
//	}
//
//	private By bookTab = By.cssSelector("li:nth-child(1).listDisplay.pe-bold.disablePointer");
//	public By getBookTab() {
//		return bookTab;
//	}
//
//	private By chapterTab = By.cssSelector("li:nth-child(2).listDisplay.pe-bold.disablePointer");
//	public By getChapterTab() {
//		return chapterTab;
//	}
//
//	private By caseTab = By.cssSelector("li:nth-child(3).listDisplay.pe-bold.disablePointer");
//	public By getCaseTab() {
//		return caseTab;
//	}
//
//	private By ReadingsTab = By.cssSelector("li:nth-child(4).listDisplay.pe-bold.disablePointer");
//	public By getReadingsTab() {
//		return ReadingsTab;
//	}
//	private By backButtonIcon = By.cssSelector(".home-link-icon>span");
//    private By leftPanelTrimSizeFilter = By.cssSelector(".facet-container:nth-child(3) .boldTxt");
//
//	private By infoIconResourcePlayer = By.cssSelector(".info-icon-wrapper>div>span>svg");
//	private By infoModal = By.id("resource-player-info-content");
//	private By infoModalAddToCollectoionButton = By.cssSelector(".pe-col-8 .pe-btn__primary--btn_large");
//	private By infoModalTitle = By.cssSelector(".title");
//	private By infoModalResourceName = By.cssSelector(".heading-note");
//	private By infoModalPublisher = By.cssSelector(".pe-col-12.pe-label:nth-child(3)");
//	private By infoModalDescription = By.cssSelector(".pe-col-12.pe-label:nth-child(4)");
//	private By infoModalPrice = By.cssSelector(".res-price>span>span");
//	private By resourcePlayerHeader = By.cssSelector(".content-meta-wrapper");
//	private By resourcePlayerBottomPagination = By.cssSelector(".content-control-left");
//	private By resourcePlayerBottomZoom = By.cssSelector(".content-control-right");
//	private By resourcePlayerZoomValue = By.cssSelector(".content-control-right>span:nth-child(3)>input");
//	private By infoModalCloseButton = By.cssSelector(".player-close-icon>a>svg");
//	private By resourcePlayerCloseButton = By.cssSelector(".close-button-wrapper>div>span>svg");

	private UIElement assignmenttoclick = createElement(UIType.Xpath, "//*[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments']/tbody/tr/th/a");
	private UIElement clickquestion = createElement(UIType.Xpath, "//*[@id='exPanel']/section[2]/ul/li[1]/span[2]/a");
	private UIElement validateplayer = createElement(UIType.ID, "xl_dijit-bootstrap_Button_0");

	public HomeworkPage(UIDriver driver) {
		super(driver);
		
	}

	 public HomeworkPage clickAssignment() {
		    try {
		    	assignmenttoclick.isDisplayed();
		    	logInstruction(assignmenttoclick.getText() + "IS DISPLAYED");
		    	
		 	 } catch (TimeOutException | WebDriverException e) {
		    	 logInstruction(e.getLocalizedMessage());
		    	 throw new TimeOutException(e.getLocalizedMessage());
		    	}
				return this;
		}
	 
	 public playerPage openPlayer() {
		    try {
		    	clickquestion.clickAndWait(5000);

		 	 } catch (TimeOutException | WebDriverException e) {
		    	 logInstruction(e.getLocalizedMessage());
		    	 throw new TimeOutException(e.getLocalizedMessage());
		    	}
				;
				return new playerPage(getUIDriver());
		}

}

