package com.xl.Instructor;

import io.netty.handler.timeout.TimeoutException;

import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.ImplicitlyWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.pearson.test.eselenium.framework.core.UIDriver;
import com.pearson.test.eselenium.framework.core.UIElement;
import com.pearson.test.eselenium.framework.exceptions.TimeOutException;
import com.pearson.test.eselenium.framework.util.UIType;
import com.thoughtworks.selenium.webdriven.commands.Click;


public class playerPage extends CommonAlertsPageObjects {

	private UIElement validateplayer = createElement(UIType.ID, "xl_dijit-bootstrap_Button_0");

	public playerPage(UIDriver driver) {
		super(driver);
		
	}

 
	 public void validateAndClosePlayer() {
		    try {
		    	Assert.assertEquals(validateplayer.isPresent(), "PLAYER NOT OPEN");
		    	validateplayer.clickByJavascript();
		    	
		 	 } catch (TimeOutException | WebDriverException e) {
		    	 logInstruction(e.getLocalizedMessage());
		    	 throw new TimeOutException(e.getLocalizedMessage());
		    	}
				;
		}

}

