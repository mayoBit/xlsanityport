package com.xl.Instructor;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;

import com.pearson.test.eselenium.framework.BasicPageObject;
import com.pearson.test.eselenium.framework.core.PageObject;
import com.pearson.test.eselenium.framework.core.UIDriver;
import com.pearson.test.eselenium.framework.core.UIElement;
import com.pearson.test.eselenium.framework.exceptions.TimeOutException;
import com.pearson.test.eselenium.framework.util.UIType;

import java.util.concurrent.TimeUnit;

public class InstructorLoginPage extends BasicPageObject implements PageObject{
    
	
	public InstructorLoginPage(UIDriver driver) {
		super(driver);
		
	}
	
	  private UIElement username1 = createElement(UIType.ID, "UserName");
	  private UIElement password1 = createElement(UIType.ID, "password");
	  private UIElement signinbtn1 = createElement( UIType.ID, "submitButton");
	
	  private UIElement usernametextbox = createElement(UIType.ID, "username");
	  private By passwordtextBox = By.id("password");
	  private UIElement signinbutton = createElement(UIType.ID, "mainButton");
	  private final UIElement checkBoxPopUp = createElement(UIType.Name,"dontshowme");
	  private final UIElement closeBtnPopUpTypeOne = createElement(UIType.Xpath,
				".//*[@id='browserCheckerMessage']/div/button");
	  private final UIElement closeBtnPopUpTypeTwo = createElement(UIType.Xpath,
				".//*[@id='browserCheckerMessage']/div[2]/div/div[1]/button");
	  
	  
	  public void closeBrowserPopup(){
		  if (checkBoxPopUp.isEnabled()) {
		      checkBoxPopUp.click();

		      if (closeBtnPopUpTypeOne.isEnabled())
		          closeBtnPopUpTypeOne.click();
		      else
		          if (closeBtnPopUpTypeTwo.isEnabled())
		              closeBtnPopUpTypeTwo.click();
		  }
		}
	  
	  public void firstLogin(){
		  if(username1.isPresent()){
		  username1.clearAndSendKeys("xl");
		  password1.clearAndSendKeys("save4later");
		  signinbtn1.clickAndWait(3000);
		  }
		  else{
			  logInstruction("CONTINUE THE TEST");
		  }
	  }
	  
	  public InstructorLoginPage enterUserName(String username) {
	    try {
	      logInstruction("LOG INSTRUCTION: ENTERING USER NAME AS : " + username);
	      usernametextbox.sendKeys(username);
	     // uiDriver.sleep(3000);
	      logInstruction("LOG INSTRUCTION: ENTERED USER NAME AS: " + username);
	    } catch (TimeOutException | WebDriverException e) {
	    		logInstruction("UNABLE TO ENTER USER NAME. \nMETHOD: enterUserName.\n" + e.getLocalizedMessage());
	    		throw new RuntimeException(e.getLocalizedMessage());
	    	}
	    	return this;
}
		 
	  public InstructorLoginPage enterPassword(String password) {
	    try {
	      logInstruction("LOG INSTRUCTION: ENTERING PASSWORD AS: " + password);
	      uiDriver.findElement(passwordtextBox).sendKeys(password);
	     // uiDriver.sleep(3000);
	      logInstruction("LOG INSTRUCTION: ENTERED PASSWORD AS: " + password);
	    } catch (TimeOutException | WebDriverException e) {
	     logInstruction("UNABLE TO ENTER PASSWORD. \nMETHOD: enterPassword.\n" + e.getLocalizedMessage());
	     throw new RuntimeException(e.getLocalizedMessage());
	    }
	    return this;
}
	  
	  public InstructorHomePage clickSignInButton() {
			try {
				signinbutton.click();
				logInstruction("LOG INSTRUCTION: CLICKED ON SIGN IN BUTTON");

			} catch (TimeOutException | WebDriverException e) {
				logInstruction(
						"UNABLE TO CLICK ON SIGN IN BUTTON. \nMETHOD: clickSignInButton.\n" + e.getLocalizedMessage());
				throw new RuntimeException(e.getLocalizedMessage());
			}
			return new InstructorHomePage(getUIDriver());

		}

}
