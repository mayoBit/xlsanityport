package com.xl.Instructor;


import java.awt.List;
import java.util.ArrayList;

import com.pearson.test.eselenium.framework.exceptions.TimeOutException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.pearson.test.eselenium.framework.core.UIDriver;

public class Controller extends InstructorHomePage {
	
	HomeworkPage homweworkpage = null;
	playerPage player = null;
	
		
	public Controller(UIDriver driver) {
		super(driver);
	}
	
	 public Controller userBasedValidation(String role, InstructorHomePage homePage) 
			 throws InterruptedException
			 
	  {
		 if (role.equalsIgnoreCase("student")){
		  		try{
					homePage.validateStudentLandingePage();
					homweworkpage = homePage.clichHomeworkTab();
					homweworkpage.clickAssignment();
//					player= homweworkpage.openPlayer();
//					player.validateAndClosePlayer();
					
				    
		  		}catch(RuntimeException e){
		  			logInstruction(("STUDENT TEST FAILED. \n"
		  		          + e.getLocalizedMessage()));
		  			throw new RuntimeException(e);
		  		  		}
		  		finally{
		  		}
			    
		  	}else if(role.equalsIgnoreCase("instructor") ){
		  		try{
		  			homePage.validateAndClickEnterPage();
		  			homePage.validateInstructorHomePage();

		  		}catch(RuntimeException e){
		  			logInstruction( e.getLocalizedMessage());
		  			throw new RuntimeException(e);
		  		}	
		  	}
			
		else{
	  		logInstruction("No role selected");
	  		}
		
		return this;
	  }
	
	 
}


