
package com.xl.Instructor;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.server.handler.FindActiveElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.openqa.selenium.WebElement;

import com.pearson.test.eselenium.framework.core.UIDriver;
import com.pearson.test.eselenium.framework.core.UIElement;
import com.pearson.test.eselenium.framework.exceptions.TimeOutException;
import com.pearson.test.eselenium.framework.util.UIType;


public class InstructorHomePage extends InstructorLoginPage {
	
	private UIElement entmathxlbutton = createElement(UIType.ID,"BtnEnter");
	private UIElement mainlogo = createElement(UIType.ID, "mainBandLogo");
	private UIElement homemanagertab = createElement(UIType.Xpath, "//*[@id='mainLeftNav']/div[11]/a");
	private UIElement homemanagertitle = createElement(UIType.ID, "PageHeaderTitle");
	private By userNameLabel = By.xpath("//*[@id='mainTopNav']/li[1]");
	private By logoutButton = By.xpath("//*[@id='mainTopNav']/li[1]/ul/li[5]");
	private UIElement homeworktab = createElement(UIType.Xpath, "//*[@id='mainLeftNav']/div[4]/a");

	public By resourceList = By.className("single-course-card instructor");
	public By title = By.cssSelector("#mainContent > div > div.view-container > div > div.pe-template__single.course-card-manager > div > div.course-card-container.instructor > div:nth-child(1) > div.course-info > a > div.pe-title.pe-title--large.course-title");
	
	
	public InstructorHomePage(UIDriver driver)
	{
	super(driver);
	}

  public void validateAndClickEnterPage() throws InterruptedException{
	  try{
		  uiDriver.waitToBeDisplayed(entmathxlbutton, 3000);
		  Assert.assertEquals(entmathxlbutton.isPresent(), true,"ENTER MATHXL BUTTON NOT DISPLAYED.");
		  entmathxlbutton.clickAndWait(3000);
	
		}
	  catch (TimeOutException | WebDriverException e) {
  		logInstruction("ENTER MATHXL BUTTON DISPLAYED.\n"
          + e.getLocalizedMessage());
  		throw new RuntimeException(e.getLocalizedMessage());
	  }
  }
  public void validateStudentLandingePage() throws InterruptedException{
	  try{
		  uiDriver.waitToBeDisplayed(mainlogo, 5000);
		  Assert.assertEquals(mainlogo.isPresent(), true,"MATHXL LOGO DOEAS NOT DISPLAYED.");
	
		}
	  catch (TimeOutException | WebDriverException e) {
          e.getLocalizedMessage();
  		throw new RuntimeException(e.getLocalizedMessage());
	  }
  }
  public void validateInstructorHomePage() throws InterruptedException{
	  try{
		  uiDriver.waitToBeDisplayed(mainlogo, 5000);
		  Assert.assertEquals(mainlogo.isPresent(), true,"MATHXL LOGO DOEAS NOT DISPLAYED.");
		  logInstruction("MATHXL LOGO DOEAS NOT DISPLAYED.");
		  homemanagertab.clickByJavascript();
		  Assert.assertEquals(homemanagertitle.getText(),"Home Page Manager");
		  logInstruction("HOME PAGE MANAGER TITLE DISPLAYED");
	
		}
	  catch (TimeOutException | WebDriverException e) {
          e.getLocalizedMessage();
  		throw new RuntimeException(e.getLocalizedMessage());
	  }
  }
  
  public HomeworkPage clichHomeworkTab() throws InterruptedException{
	  try{
		  homeworktab.clickByJavascript();
		}
	  catch (TimeOutException | WebDriverException e) {
  		 e.getLocalizedMessage();
  		throw new RuntimeException(e.getLocalizedMessage());
	  }
	  return new HomeworkPage(getUIDriver());
  }


  public InstructorHomePage clickLogout() {
	  try {
		   uiDriver.findElement(userNameLabel).click();
		   uiDriver.sleep(2000);
		   uiDriver.findElement(logoutButton).click();
		   uiDriver.sleep(2000);
	       logInstruction("LOG INSTRUCTION: CLICKED ON LOG OUT");
	   	   
	    } 
	    catch (TimeOutException | WebDriverException e) {
	    		logInstruction("UNABLE TO CLICK ON LOG OUT. \nMETHOD: clickLogout.\n"
	            + e.getLocalizedMessage());
	    		throw new RuntimeException(e.getLocalizedMessage());
	    }
	return new InstructorHomePage(getUIDriver());
	    	
 }
  
}