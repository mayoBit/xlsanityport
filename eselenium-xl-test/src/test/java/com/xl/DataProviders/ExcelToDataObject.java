package com.xl.DataProviders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.testng.SkipException;

import com.xl.Utilities.ExcelSheetDriver;

public class ExcelToDataObject {
	Object[][] data ;
	ExcelSheetDriver xlsUtil;
	HashMap<Integer,String> colDictionary;



	public ExcelToDataObject(String path) {
		try {
			xlsUtil = new ExcelSheetDriver(path);
			colDictionary = xlsUtil.getColumnDictionary();
			xlsUtil.ColumnDictionary();
			data = new Object[xlsUtil.RowCount()-1][xlsUtil.clumnCount()];
			System.out.println("LENGTH >>>"+data.length + "  Clmn Cnt >>>" + xlsUtil.clumnCount());

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

	public Object[][] getDataObject() {

		for (int rowCnt = 1; rowCnt < xlsUtil.RowCount(); rowCnt++) {
//			// Enter User Name by reading data from Excel
//			System.out.println("SIZE>>>" + colDictionary.size());
			for (int col = 0; col < colDictionary.size(); col++) {
				String s = colDictionary.get(col);
//				System.out.println("Column Dictionary >>>"+s);
				data[rowCnt-1][col] = xlsUtil.ReadCell(xlsUtil.GetCell(s), rowCnt);
			}
		}
		return data;
	}

	//Write to excel
	public void writeToExcel(String Text){
		try{
		//Read the spreadsheet that needs to be updated
		FileInputStream fsIP= new FileInputStream(new File("C:\\Excel.xls"));  
		//Access the workbook                  
		HSSFWorkbook wb = new HSSFWorkbook(fsIP);
		//Access the worksheet, so that we can update / modify it. 
		HSSFSheet worksheet = wb.getSheetAt(0); 
		// declare a Cell object
		Cell cell = null; 
		// Access the second cell in second row to update the value
		cell = worksheet.getRow(1).getCell(1);   
		// Get current cell value value and overwrite the value
		cell.setCellValue("OverRide existing value");
		//Close the InputStream  
		fsIP.close(); 
		//Open FileOutputStream to write updates
		FileOutputStream output_file = new FileOutputStream(new File("C:\\Excel.xls"));  
		 //write changes
		wb.write(output_file);
		//close the stream
		output_file.close();
		}
		catch(IOException e){
			
		}
	}
}
