package com.xl.DataProviders;


import org.testng.annotations.DataProvider;

import com.xl.Launcher.BaseTestClass;


public class InstructorDataProviders extends BaseTestClass {

	 @DataProvider(name = "loginData" )
		public static Object[][] getLoginData() {
		 return new ExcelToDataObject(BaseTestClass.excelpathQA).getDataObject();
	  }
	 
	 @DataProvider(name = "loginDatastd" )
		public static Object[][] getLoginDatastd() {
		 return new ExcelToDataObject(BaseTestClass.excelpathQAstd).getDataObject();
	  }
	 
	 @DataProvider(name = "loginpagedetails" )
		public static Object[][] getLoginPageData() {
		 return new ExcelToDataObject(BaseTestClass.excelpathfunctional).getDataObject();
	  }
	 
}


