package com.xl.Instructor;


import org.openqa.selenium.Alert;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.remote.SessionNotFoundException;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pearson.test.eselenium.framework.config.DefaultConfig;
import com.pearson.test.eselenium.framework.core.UIElement;
import com.xl.DataProviders.InstructorDataProviders;
import com.xl.Instructor.Controller;
import com.xl.Instructor.InstructorHomePage;
import com.xl.Instructor.HomeworkPage;
import com.xl.Launcher.BaseTestClass;
import com.xl.Launcher.DefaultUIDrivers;

public class InstructorLoginTest extends BaseTestClass {
	
	InstructorHomePage homePage = null;
	Controller controller = new Controller(uiDriver);
	HomeworkPage searchPage = null;
	boolean flag = true;
	String env_type ;
	String userType ;
	

	  @Test(dataProvider="loginData", dataProviderClass=InstructorDataProviders.class)
	  public void instructorLogin(String status, String username,
			  String password,String role, String url) throws Exception
	  { 
	        
		  if(status.equalsIgnoreCase("run")){
			 try{
				instLogin = (com.xl.Instructor.InstructorLoginPage) getStartPages(url,
			                browser, configInstructor.getValue("start_class"));
	//		    instLogin.firstLogin();
			    logInstruction("RUNNING TEST CASE:  TO VERIFY INSTRUCTOR LOGIN");
			    instLogin.closeBrowserPopup();
				homePage=loginInstructor(username, password);
				controller.userBasedValidation(role,homePage);
				homePage.clickLogout();
      
			  }catch(Exception e){
				  Assert.fail(e.getLocalizedMessage());
				  
			  }
			
			}else if(status.equalsIgnoreCase("skip")){
				  		System.out.println("SKIPPED MARKED TEST CASES");
				  		throw new SkipException("SKIPPING THE MARKED TEST CASES");
			}
		  }
	  
	  @Test(dataProvider="loginDatastd", dataProviderClass=InstructorDataProviders.class)
	  public void studentLogin(String status, String username,
			  String password,String role, String url) throws Exception
	  { 
	        
		  if(status.equalsIgnoreCase("run")){
			 try{
				instLogin = (com.xl.Instructor.InstructorLoginPage) getStartPages(url,
			                browser, configInstructor.getValue("start_class"));
	//		    instLogin.firstLogin();
			    logInstruction("RUNNING TEST CASE:  TO VERIFY INSTRUCTOR LOGIN");
			    instLogin.closeBrowserPopup();
				homePage=loginInstructor(username, password);
				controller.userBasedValidation(role,homePage);
				homePage.clickLogout();
      
			  }catch(Exception e){
				  Assert.fail(e.getLocalizedMessage());
				  
			  }
			
			}else if(status.equalsIgnoreCase("skip")){
				  		System.out.println("SKIPPED MARKED TEST CASES");
				  		throw new SkipException("SKIPPING THE MARKED TEST CASES");
			}
		  }
    	
}