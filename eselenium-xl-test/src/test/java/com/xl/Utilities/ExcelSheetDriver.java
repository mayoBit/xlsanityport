package com.xl.Utilities;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;



public class ExcelSheetDriver {
	static Sheet wrksheet;
	static Workbook wrkbook = null;
	static Hashtable<String, Integer> dict = null;
	static HashMap<Integer,String > dictGet = null;

	public ExcelSheetDriver(String ExcelSheetPath) throws BiffException,
			IOException {
		wrkbook = Workbook.getWorkbook(new File(ExcelSheetPath));
		wrksheet = wrkbook.getSheet("Sheet1");
	}
	public static int RowCount() {
		return wrksheet.getRows();
	}

	public static int clumnCount() {
		return wrksheet.getColumns();
	}
	
	public static String ReadCell(int column, int row) {
		return wrksheet.getCell(column, row).getContents();
	}

	public static void ColumnDictionary() {
		dict = new Hashtable<String, Integer>();
		for (int col = 0; col < wrksheet.getColumns(); col++) {
			try {
				dict.put(ReadCell(col, 0), col);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static HashMap<Integer,String> getColumnDictionary() {

		dictGet = new HashMap<Integer,String >();
		for (int col = 0; col < wrksheet.getColumns(); col++) {
			try {
				dictGet.put(col,ReadCell(col, 0));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return dictGet;
	}
	
	public static int GetCell(String colName) {
		try {
			int value;
			value = ((Integer) dict.get(colName)).intValue();
			return value;
		} catch (NullPointerException e) {
			e.printStackTrace();
			return (0);
		}
	}
}
