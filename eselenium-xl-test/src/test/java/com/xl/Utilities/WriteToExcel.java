package com.xl.Utilities;

import jxl.*;
import jxl.read.biff.BiffException;
import jxl.write.*;

import java.io.*;

import com.xl.Launcher.BaseTestClass;

public class WriteToExcel {
	WritableWorkbook writeWorkbook;
	Workbook readWorkbook;
	WritableSheet workSheet = null;
	public void setCellValue(String cellvalue) throws IOException, BiffException{
		 
		 readWorkbook = Workbook.getWorkbook(new File(BaseTestClass.excelpathQA));
		 writeWorkbook = Workbook.createWorkbook(new File(BaseTestClass.excelpathQA),readWorkbook);
		 
		 	try{
			 	System.out.println("Did excel file create?");
			 	workSheet = writeWorkbook.getSheet(0);
	            workSheet.addCell(new jxl.write.Label(5,1,""));
	            workSheet.addCell(new jxl.write.Label(5,2,""));
	            writeWorkbook.write();
	            writeWorkbook.close();
	  
		 	}catch(Exception e){
		 		e.printStackTrace();
		 	}
		 	
		 	
		}
	
	public void clearCell(){
		
		try{
			Blank b = new Blank(5,1);
			workSheet.addCell(b);
		   }
		catch(Exception e){
			System.out.println("Error in clear cell");
		}
	}
	
}