package com.xl.Launcher;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import com.pearson.test.eselenium.framework.config.Config;
import com.pearson.test.eselenium.framework.config.ConfigKeys;
import com.pearson.test.eselenium.framework.drivers.DriverLauncher;
import com.pearson.test.eselenium.framework.exceptions.InvalidUIDriverException;
import com.pearson.test.eselenium.framework.util.Browser;

public class DriversLauncher {

  private static Logger logger = Logger.getLogger(DriverLauncher.class);
  private Config config = null;

  public DriversLauncher(Config config) {
    this.config = config;

  }

  public WebDriver launchDrivers(String browserStr) {

    Browser browser;
    browserStr = browserStr.toUpperCase();
    try {
      browser = Browser.valueOf(browserStr);

    } catch (IllegalArgumentException illegalArgument) {
      logger.warn("Failed to parse browser type '" + browserStr + "', setting browser to firefox");
      browser = Browser.FIREFOX;
    }
    String driverURL = config.getValue(ConfigKeys.KEY_DRIVER_URL.getKey());
    if (driverURL == null || "".equals(driverURL))
      driverURL = "localhost";
    if ("localhost".equals(driverURL.trim()))
      try {
        return getLocalWebDriver(browser);
      } catch (MalformedURLException e1) {
        e1.printStackTrace();
      }
    else {
      if (!driverURL.contains("http"))
        driverURL = "http://" + driverURL;
      DesiredCapabilities cap = getCapabilities(browser);
      try {
        return new RemoteWebDriver(new URL(driverURL), cap);
      } catch (MalformedURLException e) {
        logger.debug(e);
      }
    }
    return null;
  }

  private WebDriver getLocalWebDriver(Browser browser) throws MalformedURLException {
   
      switch (browser) {
        case FIREFOX:

          FirefoxProfile prof = new FirefoxProfile();
          prof.setAssumeUntrustedCertificateIssuer(true);
          prof.setPreference("Browser.link.open_newwindow.restriction", 1);
          prof.setPreference("privacy.popups.policy", 0);
          return new FirefoxDriver(prof);

        case CHROME:
          String chromeDriverPath = config.getValue(ConfigKeys.KEY_CHROME_DRIVER_PATH.getKey());
          chromeDriverPath = checkDriverPath(chromeDriverPath, "/drivers/chromedriver.exe");
          System.setProperty(ConfigKeys.KEY_CHROME_DRIVER.getKey(), chromeDriverPath);
          DesiredCapabilities capabilities = DesiredCapabilities.chrome();
          ChromeOptions options = new ChromeOptions();
          options.addArguments("test-type");
          capabilities.setCapability("chrome.binary", chromeDriverPath);
          capabilities.setCapability(ChromeOptions.CAPABILITY, options);
          return new ChromeDriver(capabilities);
        case INTERNETEXPLORER:

          String ieDriverPath = System.getenv(ConfigKeys.KEY_IE_DRIVER.getKey());
          if (ieDriverPath == null) {
            ieDriverPath = config.getValue(ConfigKeys.KEY_IE_DRIVER_PATH.getKey());
            ieDriverPath = checkDriverPath(ieDriverPath, "/drivers/iedriver.exe");
          }
          System.setProperty(ConfigKeys.KEY_IE_DRIVER.getKey(), ieDriverPath);
          DesiredCapabilities capabilities1 = new DesiredCapabilities();
          capabilities1.setCapability("ignoreProtectedModeSettings", true);
          capabilities1.setCapability("ignoreZoomLevel", true);
          capabilities1.setCapability("requireWindowFocus", true);
          capabilities1.setCapability("logLevel", "FATAL");
          capabilities1.setJavascriptEnabled(true);
          capabilities1.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION,
              true);
   
          capabilities1.setCapability("ignoreProtectedModeSettings", true);

          return new InternetExplorerDriver(capabilities1);

        case SAFARI:
          DesiredCapabilities caps = new DesiredCapabilities();
          caps.setCapability("cleanSession", true);
          return new SafariDriver(caps);

        case HTMLUNIT:
          DesiredCapabilities caps1 = DesiredCapabilities.htmlUnitWithJs();
       
          caps1.setJavascriptEnabled(true);
         
          return new HtmlUnitDriver(caps1);

        default:
          return new FirefoxDriver();
     }
  }
//    } else {
//      switch (browser) {
//        case FIREFOX:
//
//          DesiredCapabilities firefoxCapabilities = DesiredCapabilities.firefox();
//          firefoxCapabilities.setCapability("platform", operatingSystem);
//          firefoxCapabilities.setCapability("version", browserVersion);
//          return new RemoteWebDriver(
//              new URL(
//                  "http://p_ces2:8b532b15-668d-48e0-abde-d8008e84f2a0@ondemand.saucelabs.com:80/wd/hub"),
//              firefoxCapabilities);
//        case CHROME:
//
//          DesiredCapabilities chromeCapabilities = DesiredCapabilities.chrome();
//
//          chromeCapabilities.setCapability("platform", operatingSystem);
//          chromeCapabilities.setCapability("version", browserVersion);
//
//          return new RemoteWebDriver(
//              new URL(
//                  "http://p_ces2:8b532b15-668d-48e0-abde-d8008e84f2a0@ondemand.saucelabs.com:80/wd/hub"),
//              chromeCapabilities);
//        case INTERNETEXPLORER:
//          DesiredCapabilities ieCapabilites = DesiredCapabilities.internetExplorer();
//          ieCapabilites.setCapability("platform", operatingSystem);
//          ieCapabilites.setCapability("version", browserVersion);
//
//          return new RemoteWebDriver(
//              new URL(
//                  "http://p_ces2:8b532b15-668d-48e0-abde-d8008e84f2a0@ondemand.saucelabs.com:80/wd/hub"),
//              ieCapabilites);
//
//        case SAFARI:
//          DesiredCapabilities safariCapabilites = DesiredCapabilities.safari();
//          safariCapabilites.setCapability("platform", operatingSystem);
//          safariCapabilites.setCapability("version", browserVersion);
//          return new RemoteWebDriver(
//              new URL(
//                  "http://p_ces2:8b532b15-668d-48e0-abde-d8008e84f2a0@ondemand.saucelabs.com:80/wd/hub"),
//              safariCapabilites);
//
//        case OPERA:
//          @SuppressWarnings("deprecation")
//          DesiredCapabilities operaCapabilites = DesiredCapabilities.opera();
//          operaCapabilites.setCapability("platform", operatingSystem);
//          operaCapabilites.setCapability("version", browserVersion);
//          return new RemoteWebDriver(
//              new URL(
//                  "http://p_ces2:8b532b15-668d-48e0-abde-d8008e84f2a0@ondemand.saucelabs.com:80/wd/hub"),
//              operaCapabilites);
//
//        default:
//          return new FirefoxDriver();
//      }
//    }
//  }

  private static DesiredCapabilities getCapabilities(Browser browser) {
    switch (browser) {
      case FIREFOX:
        return DesiredCapabilities.firefox();
      case CHROME:
        return DesiredCapabilities.chrome();
      case INTERNETEXPLORER:
        return DesiredCapabilities.internetExplorer();
      case SAFARI:
        return DesiredCapabilities.safari();
      case HTMLUNITWITHJS:
        return DesiredCapabilities.htmlUnitWithJs();
      default:
        return DesiredCapabilities.firefox();
    }
  }

  private String checkDriverPath(String path, String defaultPath) {
    if ("".equals(path) || path.startsWith("/")) {
      if ("".equals(path))
        path = defaultPath;
      URL url = getClass().getResource(path);
      if (url != null)
        path = url.getFile();
      else
        throw new InvalidUIDriverException(path);
    } else {
      File file = new File(path);
      if (!file.exists())
        throw new InvalidUIDriverException(path);
    }
    return path;
  }
}