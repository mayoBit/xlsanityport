
package com.xl.Launcher;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.SessionNotFoundException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;






//import com.collections.Admin.AccountsPage;
//import com.collections.Admin.AdminDashboardPage;
//import com.collections.Admin.AdminLoginPage;
//import com.collections.Admin.CCCOrderPage;
//import com.collections.Admin.CompletedPage;
//import com.collections.Admin.ContentAdminHomePage;
//import com.collections.Admin.FailedPage;
//import com.collections.Admin.InProgressPage;
//import com.collections.Admin.PendingApprovalPage;
//import com.collections.Admin.PendingPCSApprovalPage;
//import com.collections.Admin.PendingSPWSetupPage;
//import com.collections.Admin.ReportsPage;
//import com.collections.Admin.ResourcePage;
//import com.collections.Admin.Rights3PPage;
//import com.collections.Admin.SearchPage;
//import com.collections.Admin.TOCPage;
//import com.collections.Admin.Print.PrintBookDetails;
//import com.collections.Admin.Print.PrintBundleInfoPage;
//import com.collections.Admin.Print.PrintCollection;
//import com.collections.Admin.Print.PrintCollectionNotes;
//import com.collections.Admin.Print.PrintComponentPage;
//import com.collections.Admin.Print.PrintCoverandTitle;
//import com.collections.Admin.Print.PrintEvaluationInfo;
//import com.collections.Admin.Print.PrintOrderDetails;
//import com.collections.Admin.Print.PrintSelectCoverImage;
//import com.collections.Admin.Print.PrintSelectTextLayOut;
//import com.collections.Admin.Print.SendtofilestoPrinter;





//import com.collections.PrintVendor.PrintDetails;
//import com.collections.PrintVendor.PrintVendorHomePage;
//import com.collections.PrintVendor.SearchPrintOrders;
import com.pearson.test.eselenium.framework.config.Config;
import com.pearson.test.eselenium.framework.config.DefaultConfig;
import com.pearson.test.eselenium.framework.exceptions.TimeOutException;
import com.xl.Instructor.InstructorHomePage;


public class BaseTestClass extends BaseTestObject {

  public enum Browsers {
    FF("FIREFOX"), CHROME("CHROME"), IE("INTERNETEXPLORER");

    private final String browser;

    private Browsers(final String s) {
      browser = s;
    }

    public String getBrowser() {
      return browser;
    }
  }

  /* Object Initialization  */
  public com.xl.Instructor.InstructorLoginPage instLogin = null;
  public com.xl.Instructor.InstructorHomePage instHome = null;
  public com.xl.Instructor.HomeworkPage searchPage = null;

 
  /* Test Data Configuration */
  public static String CONFIGPATH_INSTRUCTOR_QA = "/config/instructorTestDataInQA.cfg";
  public static String CONFIGPATH_INSTRUCTOR_STAGING = "/config/instructorTestDataInStaging.cfg";
  public static String CONFIGPATH_INSTRUCTOR_STAGING_MSDN ="/config/instructorTestDataInStagingmsdn.cfg";
  public static Config configInstructor = new DefaultConfig(CONFIGPATH_INSTRUCTOR_QA);
  public static Config configInstructorstg = new DefaultConfig(CONFIGPATH_INSTRUCTOR_STAGING);
  public static Config configInstructorstgmsdn = new DefaultConfig(CONFIGPATH_INSTRUCTOR_STAGING_MSDN);
  public String instructorStagingUrl = configInstructorstg.getValue("instructorStagingUrl");
  public String instructorQAUrl = configInstructor.getValue("instructorQAUrl");
  public static String  excelpathQA = null;
  public static String  excelpathQAstd = null;
  public static String excelpathfunctional =null;
 

  public static String type ;
  public String browser_type = null;
  public static String environment_type = null;
  public String run_On_Sauce = null;
  public String user_type = null;
  public String browser = null;
  public String browser_Version_On_Saucelabs = null;


  @BeforeClass(alwaysRun = true, groups = {"Sanity", "Regression", "LoginFunctionality",
      "StudentFunctionality", "PlayerFunctionality", "Instructor_BVT"})

  @Parameters({"browser_on_sauce","envType", "userType"})

    public void beforeClass(@Optional("CHROME") String browserop,@Optional("qa") String env_type, @Optional("instructor") String userType){

	  browser = browserop;
	  user_type = userType;

env_type = "qa";
//env_type = "staging";
userType = "instructor";
//  userType = "student";

    System.out.println(env_type + "-" + userType);
    type= env_type + "-" + userType;
    setConfigFileAndStartPages();
 
  }

  public void setConfigFileAndStartPages() {
    switch (type.toLowerCase()) {
      case "qa-instructor":

        config = new DefaultConfig(CONFIGPATH_INSTRUCTOR_QA);
        excelpathQA = configInstructor.getValue("excelpathQA");
        excelpathQAstd = configInstructor.getValue("excelpathQAstd");
        excelpathfunctional = configInstructor.getValue("excelpathfunctional");

        break;
        
      case "staging-instructor":
        config = new DefaultConfig(CONFIGPATH_INSTRUCTOR_STAGING);
        excelpathQA = configInstructorstg.getValue("excelpathQA");
        excelpathfunctional = configInstructorstg.getValue("excelpathfunctional");

       break;
 
    }

    logInstruction("Environment And User Type are: " + type);
    logInstruction("Environment URL : " + environment_type);
  }


  public InstructorHomePage loginInstructor(String userName, String password) {
		
		try {
			instHome = instLogin.enterUserName(userName)
		     .enterPassword(password)
		     .clickSignInButton();
           uiDriver.manage().timeouts().implicitlyWait(25,TimeUnit.SECONDS) ;

		} catch (Exception e) {
			Assert.fail("USER LOGIN FAILED TO INSTRUCTOR PORTAL");
		}
		return  instHome;
	  }

  
  public String getFailureMessage(Throwable e) {
    StringWriter sw = new StringWriter();
    e.printStackTrace(new PrintWriter(sw));
    String exceptionAsString = sw.toString();
    return exceptionAsString;
  }

 
  @AfterClass(alwaysRun = true, groups = {"Sanity", "Regression", "LoginFunctionality",
      "StudentFunctionality", "PlayerFunctionality", "Instructor_BVT"})
  public void afterClass() {
    try {
    	DefaultUIDrivers uidrivers= new DefaultUIDrivers(uiDriver);
      uiDriver.manage().deleteCookieNamed("JSESSIONID");
      uiDriver.manage().deleteAllCookies();
      uiDriver.quit();
      uidrivers.quit();

     
    } catch (NullPointerException e) {
      logInstruction("NULL POINTER EXCEPTION - BROWSER ALREADY CLOSED");
    } catch (org.openqa.selenium.remote.UnreachableBrowserException e) {
      logInstruction("UNREACHABLE BROWSER EXCEPTION - CANNOT CLOSE THE BROWSER");
    } catch (NoSuchWindowException e) {
      logInstruction("NO SUCH WINDOW EXCEPTION - CANNOT QUIT THE BROWSER");
    } catch (SessionNotFoundException e) {
      logInstruction("NO SUCH SESSION FOUND EXCEPTION ");
    } catch (org.openqa.selenium.UnhandledAlertException e) {
      Alert javascriptAlert = uiDriver.getWebDriver().switchTo().alert();
      javascriptAlert.accept();
      uiDriver.manage().deleteAllCookies();
      uiDriver.quit();
      logInstruction("UNHANDLED ALERT EXCEPTION ");
    }
  }


  public void navigateToPageTop() {
    try {
      uiDriver.moveBrowserToXYHeightWidth(0, 0, 0, 0);
    } catch (TimeOutException | WebDriverException e) {
      throw new RuntimeException(e.getLocalizedMessage());
    }
  }



  public static String getDate() {
    String DATE_FORMAT_NOW = "yyyy-MM-d";
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
    TimeZone zone = TimeZone.getTimeZone("America/Denver");
    sdf.setTimeZone(zone);
    String noteSuffix = sdf.format(cal.getTime()).toString();

    return noteSuffix;
  }

  public static String getPreviousDate() {
    String DATE_FORMAT_NOW = "yyyy-MM-d";
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
    String noteSuffix = sdf.format(cal.getTime()).toString();

    return noteSuffix;
  }



}