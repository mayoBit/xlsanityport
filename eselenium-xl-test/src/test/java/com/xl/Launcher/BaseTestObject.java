package com.xl.Launcher;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.annotations.Listeners;

import com.google.common.base.Function;
import com.pearson.test.eselenium.framework.BasicTestObject;
import com.pearson.test.eselenium.framework.core.PageObject;
import com.pearson.test.eselenium.framework.core.UIDriver;
import com.pearson.test.eselenium.framework.exceptions.InvalidURLException;
import com.pearson.test.eselenium.framework.exceptions.StartPageClassException;
import com.pearson.test.eselenium.framework.exceptions.StartPageURLException;
import com.pearson.test.eselenium.framework.testlink.BasicTestlinkTestObject;

@SuppressWarnings("deprecation")
//@Listeners(TestLinkReporterChild.class)
public class BaseTestObject extends BasicTestlinkTestObject {

  private static Logger logger = Logger.getLogger(BasicTestObject.class);

  public BaseTestObject() {
    super();
  }

  /*
   * public BaseTestObject(UIDriver driver) { super(driver); }
   */

  public PageObject getStartPages(final String pageurl, String browser, String pageclass) {

    if (pageclass == null && "".equals(pageclass)) {
      logger.error(new StartPageClassException(""));
      return null;
    }
    try {
      // pageurl = ParameterChecker.checkURL(pageurl);
    } catch (InvalidURLException e) {
      logger.error("Failed while checking URL", new StartPageURLException(pageurl));
      return null;
    }

    if (uiDriver == null)
      uiDriver = new DefaultUIDrivers(browser);

    uiDriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);


    try {
      logger.debug("Maximizing Window");
      uiDriver.maximizeWindow();

      logger.debug("Go to URL: " + pageurl);
      uiDriver.get(pageurl);

      // trying to avoid webdriver timeout issue
    } catch (RuntimeException e) {
      logger.error("Failed to go to URL: " + pageurl);

      new FluentWait<UIDriver>(uiDriver).withTimeout(30, TimeUnit.SECONDS)
          .pollingEvery(20, TimeUnit.SECONDS).until(new Function<UIDriver, Boolean>() {
            @Override
            public Boolean apply(UIDriver d) {
              try {
                logger.debug("Trying to navigate to URL again: " + pageurl);
                ((JavascriptExecutor) d).executeScript("window.stop();");
                ((JavascriptExecutor) d).executeScript("location.reload();");
                return ((JavascriptExecutor) d).executeScript("return document.readyState")
                    .equals("complete");

              } catch (RuntimeException e2) {
                logger.error("Failed to go to URL: " + pageurl);
              }
              return false;

            }
          });



    }

    try {
      @SuppressWarnings("unchecked")
      Class<PageObject> clazz = (Class<PageObject>) Class.forName(pageclass);
      PageObject pageObject = clazz.getDeclaredConstructor(UIDriver.class).newInstance(uiDriver);
      pageObject.setUrl(pageurl);
      return pageObject;
    } catch (ClassNotFoundException e) {
      StartPageClassException ex = new StartPageClassException("");
      logger.error(ex);
      throw ex;
    } catch (InstantiationException e) {
      logger.error("InstantiationException", e);
    } catch (IllegalAccessException e) {
      logger.error("IllegalAccessException", e);
    } catch (IllegalArgumentException e) {
      logger.error("IllegalArgumentException", e);
    } catch (SecurityException e) {
      logger.error("SecurityException", e);
    } catch (InvocationTargetException e) {
      logger.error(e);
    } catch (NoSuchMethodException e) {
      logger.error(e);
    }
    return null;
  }

}